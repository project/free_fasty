
INTRODUCTION
------------
About Fasty!
Fasty is a free trial of the module that hundreds of sites use to optimise site load speed and increase conversions at least 3X.

No changes are made on your website, our code works in background improving on network communication layer.

You just need 1 module: Enhance Drupal's existing best in class infrastructure. No need for additional CDN No developer required Positive TTFB (time to first byte) impact See apps speed impact fast Mobile, tablet and desktop page speed improved SEO Speed-Up

Improve Conversion Rate Improving site performance improves website conversion rate. A fast loading shop means happier customers with speedy checkout who don’t go to your competitors!

Monitor Site Speed Improvements Onload loading time in seconds. Monitor changes and improvements to your site performance.

Expert Support

RECOMMENDED MODULES
-------------------

 * No extra module is required.

INSTALLATION
------------

 * Install as usual

CONFIGURATION
-------------

 * Configuration page can be found here.
   Path - admin/config/services/fastyweb